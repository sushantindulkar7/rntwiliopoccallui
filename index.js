/**
 * @format
 */

import {AppRegistry, Platform} from 'react-native';

import App from './App';
import {RNTwilioPhone} from 'react-native-twilio-phone';
import {name as appName} from './app.json';

if (Platform.OS === 'android') {
  RNTwilioPhone.handleBackgroundState();
}

AppRegistry.registerComponent(appName, () => App);
