/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import {
  ActivityIndicator,
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  TextInput,
  View,
  useColorScheme,
} from 'react-native';
import {Colors, Header} from 'react-native/Libraries/NewAppScreen';
import {
  EventType,
  RNTwilioPhone,
  twilioPhoneEmitter,
} from 'react-native-twilio-phone';

import RNCallKeep from 'react-native-callkeep';
import React from 'react';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const callKeepOptions = {
    ios: {
      appName: 'TwilioPhone Example',
      supportsVideo: false,
    },
    android: {
      alertTitle: 'Permissions required',
      alertDescription: 'This application needs to access your phone accounts',
      cancelButton: 'Cancel',
      okButton: 'OK',
      additionalPermissions: [],
      // Required to get audio in background when using Android 11
      foregroundService: {
        channelId: 'com.example.reactnativetwiliophone',
        channelName: 'Foreground service for my app',
        notificationTitle: 'My app is running on background',
      },
    },
  };

  async function fetchAccessToken() {
    const response = await fetch(
      'https://fa29-206-183-111-25.ngrok.io/token/generate',
    );
    // const response = await fetch('https://fresh-moth-83.loca.lt/accessToken');

    //https://angry-duck-92.loca.lt/accessToken
    //https://fresh-moth-83.loca.lt/accessToken my

    // console.log(
    //   'url =',
    //   'http://fa29-206-183-111-25.ngrok.io/accessToken?identity=' +
    //     identity +
    //     '&os=' +
    //     Platform.OS,
    // );
    const accessToken = await response.json();
    // const accessToken = await response.text();

    console.log('accessToken ===111', accessToken);
    // console.log('accessToken ===2', accessToken.token);

    // return accessToken;
    return accessToken.token;
  }

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [to, setTo] = React.useState('');
  const [callInProgress, setCallInProgress] = React.useState(false);

  console.log(RNTwilioPhone.calls);

  //RNTwilioPhone options
  const options = {
    requestPermissionsOnInit: true, // Default: true - Set to false if you want to request permissions manually
  };

  React.useEffect(() => {
    // return RNTwilioPhone.initialize(callKeepOptions, fetchAccessToken, options);
    return RNTwilioPhone.initializeCallKeep(
      callKeepOptions,
      fetchAccessToken,
      options,
    );
  }, []);

  React.useEffect(() => {
    const subscriptions = [
      twilioPhoneEmitter.addListener(EventType.CallConnected, data1 => {
        console.log('data1,', data1);
        setCallInProgress(true);
      }),
      twilioPhoneEmitter.addListener(EventType.CallDisconnected, data2 => {
        console.log('data2=', data2);
        setCallInProgress(RNTwilioPhone.calls.length > 0);
      }),
      twilioPhoneEmitter.addListener(EventType.CallDisconnectedError, data => {
        console.log('data3', data);
        setCallInProgress(RNTwilioPhone.calls.length > 0);
      }),
    ];

    return () => {
      subscriptions.map(subscription => {
        subscription.remove();
      });
    };
  }, []);

  function hangup() {
    // RNCallKeep.endAllCalls();
    RNCallKeep.endCall(to);
    setCallInProgress(false);
  }

  async function call() {
    if (to === '') {
      return;
    }

    setCallInProgress(true);

    try {
      // console.log('RNTwilioPhone =', JSON.stringify(RNTwilioPhone));
      // const accessToken = await RNTwilioPhone.fetchAccessToken();
      // console.log('===', accessToken);
      // await RNTwilioPhone.startCall(
      //   '09757122596',
      //   'My friend',
      //   'client:+19287233851',
      // );
      await RNTwilioPhone.startCall(to, 'My friend', '+17057102594');
      // await RNTwilioPhone.startCall('+918446956072');
    } catch (e) {
      console.log(e);
      setCallInProgress(false);
    }
  }

  async function unregister() {
    try {
      await RNTwilioPhone.unregister();
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          {/* <Section title="Step One">
            Edit <Text style={styles.highlight}>App.js</Text> to change this
            screen and then come back to see your edits.
          </Section>
          <Section title="See Your Changes">
            <ReloadInstructions />
          </Section>
          <Section title="Debug">
            <DebugInstructions />
          </Section>
          <Section title="Learn More">
            Read the docs to discover what to do next:
          </Section>
          <LearnMoreLinks /> */}
          <View style={styles.container}>
            {callInProgress ? (
              <View>
                <ActivityIndicator color="#999" style={styles.loader} />
                <Button title="End call" onPress={hangup} />
              </View>
            ) : (
              <View>
                <TextInput
                  style={styles.to}
                  onChangeText={text => setTo(text)}
                  value={to}
                  placeholder="Client or phone number"
                  placeholderTextColor="gray"
                />
                <Button title="Start call" onPress={call} />
                <View style={styles.unregister}>
                  <Button title="Unregister" onPress={unregister} />
                </View>
              </View>
            )}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loader: {
    marginBottom: 40,
  },
  to: {
    height: 50,
    width: 200,
    fontSize: 16,
    borderColor: 'gray',
    borderBottomWidth: 1,
    marginBottom: 40,
    color: 'gray',
    textAlign: 'center',
  },
  unregister: {
    marginTop: 40,
  },
});

export default App;
